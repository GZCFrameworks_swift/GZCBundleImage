# GZCBundleImage

Bundle图片管理，用于快速获取Bundle的图片，同时内部有自己的缓存机制，当BundleImage对象被释放时，缓存也会被释放

## 安装

GZCBundleImage 支持[CocoaPods](https://cocoapods.org)安装
在Podfile中添加下面的代码后，执行`pod install` :

```
pod 'GZCBundleImage'
```

## 使用
在模块中新建用于图片管理的XXXImageTool：
```
class XXXImageTool {
    let imageBundle = GZCBundleImage(resource: "XXX", customClass: XXXImageTool.self)
    
    /// 获取路径图片
    func bundleImage(_ keyPath: String) -> UIImage? {
        return imageBundle.bundleImage(keyPath)
    }
    
    // 建议使用单例的形式
    fileprivate static var _sharedInstance: XXXImageTool?
    class func shared() -> XXXImageTool {
        guard let instance = _sharedInstance else {
            _sharedInstance = XXXImageTool()
            return _sharedInstance!
        }
        return instance
    }
    private init(){}
    
    /// 清除缓存（可在离开模块时手动调用，释放内存）
    static func releaseMemory() {
        _sharedInstance?.imageBundle.cleanMemory()
        _sharedInstance = nil
    }
}
```
可设置缓存的最大值（单位是bytes）：
```
imageBundle.totalCostLimit = 200 * 1000 * 1000
```


## Author

Guo ZhongCheng, guozhongcheng@vv.cn

## License

GZCBundleImage is available under the MIT license. See the LICENSE file for more info.
