#
# Be sure to run `pod lib lint GZCBundleImage.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'GZCBundleImage'
  s.version          = '0.1.1'
  s.summary          = 'Bundle图片管理'

  s.description      = <<-DESC
  Bundle图片管理，用于快速获取Bundle的图片，同时内部有自己的缓存机制，当BundleImage对象被释放时，缓存也会被释放
                       DESC

  s.homepage         = 'https://gitlab.com/GZCFrameworks_swift/GZCBundleImage'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Guo ZhongCheng' => 'gzhongcheng@qq.com' }
  s.source           = { :git => 'https://gitlab.com/GZCFrameworks_swift/GZCBundleImage.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'
  s.swift_version = "5.1"
  
  s.default_subspec = "Framework"
  
  # 源码依赖
  s.subspec "File" do |ss|
    ss.source_files = 'GZCBundleImage/Classes/**/*'
  end
  
  # Framework包依赖, 使用打包脚本打包后可用
  s.subspec "Framework" do |ss|
    ss.vendored_frameworks = 'GZCBundleImage/PackageFramework/GZCBundleImage.framework'
  end
  
# 使用Framework（默认）方式时，如果pod install过程中提示target has transitive dependencies that include static binaries:
# 可在podfile文件里加上下面这句话再试试
# pre_install do |installer| Pod::Installer::Xcode::TargetValidator.send(:define_method, :verify_no_static_framework_transitive_dependencies) {}
# end

end
